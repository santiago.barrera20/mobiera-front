import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserModel } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { HeaderNavService } from '../services/header-nav.service';
import { MobieraStorage } from '../services/mobiera-storage.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  personalInfoForm: FormGroup
  passwordForm: FormGroup
  currentUser: UserModel
  constructor(
    public headerServices: HeaderNavService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private userService: UserService,
    private router: Router,
    private mobieraStorage: MobieraStorage,
    private authService: AuthService,

  ) {
    this.personalInfoForm = this.formBuilder.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required]
    })
    this.passwordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.headerServices.title = 'Profile'
      this.headerServices.subtitle = 'Edit your profile'
    }, 200);
    this.currentUser = new UserModel()
    this.getProfile()
  }

  getProfile() {
    const user = new UserModel()
    user.id = this.mobieraStorage.getItem('DATAUSER', {}).id
    this.userService.getUser(user).subscribe(res => {
      this.currentUser.id = res.id
      this.currentUser.password = res.password
      this.personalInfoForm.patchValue({ username: res.username })
      this.personalInfoForm.patchValue({ name: res.name })
      this.personalInfoForm.patchValue({ phone: res.phone })
      this.personalInfoForm.patchValue({ email: res.email })
    })
  }

  save(type): void {
    switch (type) {
      case 'personalinfo':
        this.personalInfoForm.markAllAsTouched()
        if (this.personalInfoForm.valid) {
          let user = new UserModel()
          user = this.personalInfoForm.value
          user.id = this.currentUser.id
          this.userService.updateUser(user).subscribe(res => {
            if (res) {
              this.showSnackbar('User profile updated successfully')
              let userdata = this.mobieraStorage.getItem('DATAUSER', {})
              userdata.username = user.username
              this.mobieraStorage.setItem('DATAUSER', userdata)
              this.router.navigate(['/dashboard'])
            } else {
              this.showSnackbar('Oops, something is wrong, try again later')
            }
          })
        } else {
          this.showSnackbar('Personal Information invalid or incomplete')
        }
        break;
      case 'password':
        if (this.passwordForm.valid) {
          if (this.currentUser.password === this.authService.encondeSHA(this.passwordForm.get('currentPassword').value)) {
            if (this.passwordForm.get('newPassword').value === this.passwordForm.get('confirmPassword').value) {
              // update password
              const pass = new UserModel()
              pass.password = this.authService.encondeSHA(this.passwordForm.get('newPassword').value)
              pass.id = this.currentUser.id

              this.userService.updateUser(pass).subscribe(res => {
                if (res) {
                  this.showSnackbar('Password updated successfully')
                  this.router.navigate(['/dashboard'])
                } else {
                  this.showSnackbar('Oops, something is wrong, try again later')
                }
              })
            } else {
              this.showSnackbar('New password do not match')

            }
          } else {
            this.showSnackbar('Current password is not valid')
          }
        } else {
          this.showSnackbar('Empty passwords')
        }
        break;
      default:
        break;
    }
  }

  showSnackbar(val): void {
    this.snackBar.open(val, null, {
      duration: 3000
    })
  }

}
