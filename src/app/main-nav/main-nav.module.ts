import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { DashboardComponent } from './../dashboard/dashboard.component'
import { MaterialModule } from '../shared/material.module';
import { AuthService } from '../services/auth.service';
import { AuthGuardGuard } from '../auth/auth-guard.guard';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';
import { MainNavComponent } from './main-nav.component';


@NgModule({
  declarations: [
    MainNavComponent,
    HeaderNavComponent,
    SideNavComponent,
    DashboardComponent,
    EditProfileComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule
  ],
  exports: [
    MainNavComponent,
    HeaderNavComponent,
     SideNavComponent,
     DashboardComponent,
     EditProfileComponent
    ],
  providers: [
    AuthService,
    AuthGuardGuard
  ]
})
export class MainNavModule { }
