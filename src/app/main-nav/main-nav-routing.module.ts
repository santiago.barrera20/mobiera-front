import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MainNavComponent } from './main-nav.component';
import { AuthGuardGuard } from '../auth/auth-guard.guard'
import { DashboardComponent } from '../dashboard/dashboard.component';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';


const routes: Routes = [{
  path: '', component: MainNavComponent
  , children: [
    // { path: '', redirectTo: '/dashboard', pathMatch:'full'},
    // { path: 'dashboard', component:DashboardComponent},
    // { path: 'profile', component: EditProfileComponent},
  ]
}]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: []
})
export class MainNavRoutingModule { }
