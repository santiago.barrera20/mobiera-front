import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { HeaderNavService } from 'src/app/services/header-nav.service';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.scss']
})
export class HeaderNavComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    public headerServices: HeaderNavService,

  ) { }

  ngOnInit(): void {
  }

  logout(): void {
    this.authService.updateLoggedInStatus(false).subscribe()
  }

  goToDashboard() {
    this.router.navigate(['/'])
  }
}
