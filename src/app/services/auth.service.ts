import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators'
import { UserModel } from '../models/user.model'
import { MobieraStorage } from '../services/mobiera-storage.service'
import { Router } from '@angular/router';
import { sha256, sha224 } from 'js-sha256';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient,
    private mobieraStorage: MobieraStorage,
    private router: Router
  ) { }

  login(user: UserModel): Observable<boolean> {
    const endpoint = environment.fakeServer.url + '/users?username=' + user.username + '&password=' + user.password
    return this.httpClient.get<UserModel[]>(endpoint).pipe(map(response => {
      if (response.length > 0) {
        const datauser = new UserModel()
        datauser.username = response[0].username
        datauser.id = response[0].id
        // datauser.auth.token = this.makeid(50)
        this.mobieraStorage.setItem('datauser', datauser)
        this.updateLoggedInStatus(true).subscribe(res => { })
        return true
      } else {
        return false
      }

    }))
  }

  isLoggedIn(): Observable<boolean> {
    if (this.mobieraStorage.getItem('loggedIn', false)) {
      return of(true)
    } else {
      return of(false)
    }
    // let user = new UserModel()
    // user = this.mobieraStorage.getItem('datauser', {})
    // const endpoint = environment.fakeServer.url + '/users?username=' + user.username + '&auth.loggedIn=true'
    // return this.httpClient.get<UserModel[]>(endpoint).pipe(map(response => {
    //   let ans = false
    //   response.length > 0 ? ans = true : ans = false
    //   return ans
    // }))
  }

  updateLoggedInStatus(status: boolean): Observable<boolean> {
    // const endpoint = environment.fakeServer.url + '/users/' + id
    // const body = {
    //   "auth": {
    //     "loggedIn": status
    //   }
    // }
    // return this.httpClient.patch<UserModel[]>(endpoint, body).pipe(map(response => {
    //   if (response.length > 0) {
    //     return true
    //   } else {
    //     return false
    //   }
    // }));
    if (status) {
      this.mobieraStorage.setItem('loggedIn', true)
      return of(true)
    } else {
      this.router.navigateByUrl('/login')
      this.mobieraStorage.removeItem('LOGGEDIN')
      return of(false)
    }

  }

  makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  encondeSHA(value):string{
    return sha256(value)
  }



}
