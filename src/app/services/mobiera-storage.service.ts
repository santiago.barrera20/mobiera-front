import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class MobieraStorage {
  setItem(key: string, data: any): void {
    // console.log(key, data)
    if (data === false || data === 0 || data === '' || !!data) {
      localStorage.setItem(key.toUpperCase(), JSON.stringify(data))
    } else {
      localStorage.removeItem(key.toUpperCase())
    }
  }

  getItem(key: string, def = null): any {
    const data = localStorage.getItem(key.toUpperCase())
    // console.log(key, {data, def})
    if (!!data) {
      try {
        return JSON.parse(data)
      } catch (err) {
        return def
      }
    } else {
      return def
    }
  }

  removeItem(key: string): void {
    localStorage.removeItem(key.toUpperCase())
  }
}
