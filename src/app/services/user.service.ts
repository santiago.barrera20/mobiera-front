import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators'
import { UserModel } from '../models/user.model'
import { MobieraStorage } from '../services/mobiera-storage.service'
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(
        private httpClient: HttpClient,
        private mobieraStorage: MobieraStorage,
        private router: Router
    ) { }

    getUser(user: UserModel): Observable<UserModel> {
        const endpoint = environment.fakeServer.url + '/users/' + user.id
        return this.httpClient.get<UserModel>(endpoint).pipe(map(response => {
            if (response !== {}) {
                return response
            } else {
                return null
            }
        }))
    }
    updateUser(user: UserModel): Observable<boolean> {
        const endpoint = environment.fakeServer.url + '/users/' + user.id
        return this.httpClient.patch<UserModel>(endpoint, user).pipe(map(response => {
            if (response !== {}) {
                return true
            } else {
                return false
            }

        }))
    }



}
