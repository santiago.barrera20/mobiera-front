import { Injectable } from '@angular/core'
@Injectable({
  providedIn: 'root'
})
export class HeaderNavService {
  title: string
  subtitle: string
}
