export class UserModel{
    id?:number
    username?:string
    password?:string
    name?:string
    email?:string
    phone?:string
    auth?:Auth = new Auth()

    constructor(){
    }
}

export class Auth{
    loggedIn?:boolean
    token?:string
}