import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from './models/user.model';
import { AuthService } from './services/auth.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mobiera-front';

  constructor(
    private authService: AuthService,
    private router: Router
  ){

  }

  ngOnInit(): void {
    this.authService.isLoggedIn().subscribe(res=>{
      console.log('loggedIn: '+res);
      res?this.router.navigateByUrl('/'):this.authService.updateLoggedInStatus(false).subscribe(res=>{})
    })
  }
}
