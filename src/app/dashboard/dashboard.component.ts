import { Component, OnInit } from '@angular/core';
import { HeaderNavService } from '../services/header-nav.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    public headerServices: HeaderNavService,

  ) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.headerServices.title='Dashboard'
      this.headerServices.subtitle='Detailed Analytics'
    }, 200);
  }

}
