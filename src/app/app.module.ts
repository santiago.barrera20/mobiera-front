import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavModule } from './main-nav/main-nav.module';
import { HttpClientModule } from '@angular/common/http';
import {SharedModule} from './shared/shared.module'
import { AuthGuardGuard } from './auth/auth-guard.guard';
import { AuthComponent } from './auth/auth.component';
import { MaterialModule } from './shared/material.module';
import { MainNavRoutingModule } from './main-nav/main-nav-routing.module';
import { CommonModule } from '@angular/common';
import { MainNavComponent } from './main-nav/main-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    // MainNavComponent,
    AuthComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    MainNavRoutingModule,
  ],
  providers: [AuthGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
