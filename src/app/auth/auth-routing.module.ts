import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from './auth-guard.guard';


const routesAuth: Routes = [

  { path: 'login', component: LoginComponent }

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routesAuth)
  ],
  exports: [RouterModule],
  providers: [AuthGuardGuard]
})
export class AuthRoutingModule { }
