import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from '../auth/login/login.component';
import { AuthComponent } from './auth.component';
import { AuthService } from '../services/auth.service';
import { AuthGuardGuard } from './auth-guard.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material.module';


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule
  ],
  // exports:[AuthComponent,LoginComponent],
  providers: [
    AuthService,
    AuthGuardGuard
  ]
})
export class AuthModule { }
