import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup
  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router

  ) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }
  onLogin(): void {
    this.form.markAllAsTouched()
    if (this.form.valid) {
      const user = new UserModel()
      user.username = this.form.get('username').value.toLowerCase()
      user.password = this.authService.encondeSHA(this.form.get('password').value)
      this.authService.login(user).subscribe(res => {
        res? this.router.navigate(['/']): this.showSnackbar('invalid user or password');
      })
    } else {
      this.showSnackbar('Empty parameters')
    }
  }

  showSnackbar(val): void {
    this.snackBar.open(val, null, {
      duration: 3000
    })
  }



}
