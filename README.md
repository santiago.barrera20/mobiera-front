# MobieraFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.

## Development server 🚀

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Fake database server ⚙️

To run fake database install json-server
```
npm install -g json-server
```

Then run the fake server in the same angular project route. Run 
```
npm install -g json-server
```

Finally run the fake server
```
json-server --watch db.json
```

## AUTH CREDENTIALS
User: sb
Password: 123456